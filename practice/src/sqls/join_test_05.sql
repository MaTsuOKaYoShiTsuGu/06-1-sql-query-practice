/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
select concat(a.lastName,', ',a.firstName) as employee, concat(b.lastName,', ',b.firstName) as manager from
employees a join employees b on a.reportsTo=b.employeeNumber
order by b.lastName,b.firstName,a.lastName,a.firstName