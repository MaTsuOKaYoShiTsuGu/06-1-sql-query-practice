/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
/*
select minOrderItemCount,maxOrderItemCount,avgOrderItemCount from

*/
select min(cnt) as minOrderItemCount,max(cnt) as maxOrderItemCount,floor(avg(cnt)) as avgOrderItemCount
from(select COUNT(*) as cnt from orderdetails group by orderNumber) as otherName